    <footer>
        <div class="container-fluid nin">
            <div class="grid1">
                <div class="container-fluid col_left">
                    <div class="container-fluid sub_grid">
                        <div class="container-fluid sub_col_left">
                            <h2>About</h2>
                            <li><a href="#">History</a></li>
                            <li><a href="#">Our Team</a></li>
                            <li><a href="#">Brand Guidelines</a></li>
                            <li><a href="#">Terms& Condition</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </div>
                        <div class="container-fluid sub_col_right">
                            <h2>Services</h2>
                            <li><a href="#">How to Order</a></li>
                            <li><a href="#">Our Product</a></li>
                            <li><a href="#">Order Status</a></li>
                            <li><a href="#">Promo</a></li>
                            <li><a href="#">Payment Method</a></li>
                        </div>
                    </div>
                </div>
                <div class="container-fluid col_right">
                    <h2>Title Here</h2>
                    <h3>Lorem Ipsum available, but the majorit</h3>
                    <div class=" telegram">
                        <a href="#">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/img20.png" alt="Telegram buton">
                        </a>
                    </div>
                    <div class="social_media">
                        <button class="ig">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/instagram.png" alt="Instagram button">
                        </button>
                        <button class="fb">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/facebook.png" alt="Facebook button">
                        </button>
                        <button class="tw">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/twitter.png" alt="Twitter button">
                        </button>
                        <button class="wp">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/whatsapp.png" alt="Whatsapp button">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>