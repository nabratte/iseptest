 <?php //wp_nav_menu( array( 'header-menu' => 'header-menu' ) ); ?>


 <!DOCTYPE html>
<html <?php language_attributes(); ?>>
 <head>
   <title><?php bloginfo('name'); ?> &raquo; <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/css/bootstrap.min.css">
   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?v=1.0">
   <?php wp_head(); ?>
 </head>
<body <?php body_class(); ?>>
    <header>
        <nav class="navbar navbar-expand-md">
            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/logo.png" alt="Logo de la empresa">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class= "col-9 container-fluid menu">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" >Service</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
              </ul>
                <div class="col-4 container-fluid search-container">
                    <form class="search-form mt-2 mt-md-0">
                        <input class="searcher-form mr-sm-2" type="text" placeholder="Search.." aria-label="Search">
                        <button class= "search" >
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/wp-IsepTest/img/SearchButton.png" alt="Search Button">
                        </button>
                    </form>
                </div> 
            </div>
        </nav>
    </header>